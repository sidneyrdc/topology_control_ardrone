/******************************************************************************
 * Extra functions to network topology control algorithm <Source>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2017 Out 11 18:45:03
 * Info: This file contains the implementation  of the auxiliary functions used
 * in the network topology control algorithm.
 *****************************************************************************/

#include <utils.hpp>

/*
 * Geodetic Transformation
 *
 * Info: transform from geodetic (latitude, longitude, ellipsoidal heigh) to
 * geocentric coordinates (X, Y, Z). Geodetic are obtained from WGS84 and geocentric
 * are obtained from ECEF.
 */
pose_t lla2ecef(gps_t gps_data) {
    // geocentric coordinates
    pose_t geo_data;

    // convert latitude and longitude from degree to radian
    gps_data.latitude *= PI/180.0;
    gps_data.longitude *= PI/180.0;

    // normal of a latitude
    const double N = geo_norm(gps_data.latitude);

    // X coordinate on ECEF
    geo_data.x = (N + gps_data.altitude)*cos(gps_data.latitude)*cos(gps_data.longitude);

    // Y coordinate on ECEF
    geo_data.y = (N + gps_data.altitude)*cos(gps_data.latitude)*sin(gps_data.longitude);

    // Z coordinate on ECEF
    geo_data.z = (pow(GEO_MIN_AXIS, 2)/pow(GEO_MAJ_AXIS, 2)*N + gps_data.altitude)*sin(gps_data.latitude);

    return geo_data;
}

/*
 * Normal of a latitude
 *
 * Info: calculates the distance from the earth's surface to the Z-axis along
 * the ellipsoid normal. lat is the latitude and must be provided in radians.
 */
double geo_norm(float lat) {
    return pow(GEO_MAJ_AXIS, 2)/sqrt(pow(GEO_MAJ_AXIS, 2)*pow(cos(lat), 2) + pow(GEO_MIN_AXIS, 2)*pow(sin(lat), 2));
}

/*
 * Geodetic data to local coordinates
 *
 * Info: transform from geodetic (latitude, longitude, altitude) to local
 * Cartesian coordinates (X, Y, Z).
 */
pose_t lla2xyz(gps_t gps_data) {
    // local cartesian coordinates
    pose_t cart_data;

    // calculates the distance between the base reference the current point
    gps_data.longitude != 0 ? cart_data.x = (gps_data.longitude - BASE_LON)*LON_RADIUS*PI/180.0 : cart_data.x = 0;
    gps_data.latitude != 0 ? cart_data.y = (gps_data.latitude - BASE_LAT)*LAT_RADIUS*PI/180.0 : cart_data.y = 0;
    cart_data.z = gps_data.altitude;

    return cart_data;
}

/*
 * Compass angle to cartesian angles
 *
 * Info: Converts angles from compass referential to cartesian referential.
 */
float comp2cart(float comp_angle) {
    // cartesian angle
    float cart_angle = 90 - comp_angle;

    // normalize angles between 0 and 360
    cart_angle < 0 ? cart_angle += 360 : 0;

    return cart_angle;
}

