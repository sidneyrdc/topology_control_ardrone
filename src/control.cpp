/******************************************************************************
 * Motion Control Algorithms <Implementation>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2017 Mar 08 15:10:40
 * Info: This file contains the implementation of the motion control algorithms
 *****************************************************************************/

#include <control.hpp>

using namespace arma;
using namespace QuadProgPP;

/* class constructor */
control::control(unsigned int id,
                 float p,
                 float dt,
                 float ux_max,
                 float ux_min,
                 float ua_max,
                 float ua_min,
                 vec r_com,
                 vec r_cov,
                 vec gamma) {

    // set initial control parameters
    this->id = id;
    this->p = p;
    this->dt = dt;
    this->ux_max = ux_max;
    this->ux_min = ux_min;
    this->ua_max = ua_max;
    this->ua_min = ua_min;
    this->r_com = r_com;
    this->r_cov = r_cov;
    this->gamma = gamma;

    // auxiliary matrices to build the mpc formulation
    this->T = trimatl(ones(p, p));
    this->T2 = T*T;
    this->I = eye(p, p);
    this->P = diagmat(linspace(1, p, p));
    this->TI = I;  //this->TI = inv(T); // for some reason in x64 platform LAPACK support is damaged
    for(unsigned int i = 0; i < p - 1; i++) TI(i + 1, i) = -1;
}

/* class destructor */
control::~control() {
    //NOP
}

/* first order MPC motion controller */
vec control::mpc_1st_order(mat x, mat u, uvec A, uvec H) {
    // dimensions of the control array
    const unsigned int dim = 3;

    // number of elements in X (we have p predictions for each dimension)
    const unsigned int n = p*dim;

    // number of elements in ci0 (2 = we have ub and lb for all predictions)
    const unsigned int m = 2*n;

    // get 1-hop neighbors of id
    //const uvec N1 = find(A);

    // get the real and virtual neighbours of id
    const uvec N = find(max(A, H));

    /*-------------------------------------------------------------------------
    Quadratic optimization form:
    min 0.5 * x G x + g0 x
    s.t.
        CE^T x + ce0 = 0
        CI^T x + ci0 >= 0

    The matrix and vectors dimensions are as follows:
        G: n * n
        g0: n

        CE: n * p
        ce0: p

        CI: n * m
        ci0: m

        x: n
    -------------------------------------------------------------------------*/

    // main matrices of quadratic formulation (using quadprog++ matrix type)
    Matrix<double> G(n, n), CE, CI(n, m);
    Vector<double> g0(n), U(n), ce0, ci0(m);

    // temporary matrices to make formulation
    mat Hxy = zeros(p, p);          // hessian matrix to x and y coordinates
    mat Htheta = zeros(p, p);       // hessian matrix to θ angle (YAW)
    mat gxy = zeros(2, p);          // gradient array to x and y coordinates
    rowvec gtheta = zeros<rowvec>(p);       // gradient array to θ angle (YAW)

    // initial positions and control signals
    mat U_i0 = zeros(p, dim);
    mat U_j = zeros(p, dim);
    mat X_i = zeros(p, dim);
    mat X_j = zeros(p, dim);
    mat X_d = zeros(p, dim);

    // zero fill
    G = 0;
    U = 0;
    CI = 0;
    ci0 = 0;

    // fill control variable
    U_i0(0, span()) = u(id, span());

    // fill state array
    X_i = repmat(x(id, span()), p, 1);

    // build the control signal saturation constraints
    for(unsigned int i = 0; i < p; i++) {
        CI[i][i] = -1;              // -ux + ux_max >= 0
        CI[i][i+p] = 1;             //  ux - ux_min >= 0
        CI[i+p][i+p*2] = -1;        // -uy + uy_max >= 0
        CI[i+p][i+p*3] = 1;         //  uy - uy_min >= 0
        CI[i+p*2][i+p*4] = -1;      // -uθ + uθ_max >= 0
        CI[i+p*2][i+p*5] = 1;       //  uθ - uθ_min >= 0

        ci0[i] = ux_max;            //  ux_max
        ci0[i+p] = -ux_min;         // -ux_min
        ci0[i+p*2] = ux_max;        //  uy_max
        ci0[i+p*3] = -ux_min;       // -uy_min
        ci0[i+p*4] = ua_max;        //  uθ_max
        ci0[i+p*5] = -ua_min;       // -uθ_min
    }

    // build the control matrices for each neighbor of i
    for(unsigned int j = 0; j < N.n_elem; j++) {
        // fill neighbor state and control arrays
        X_j = repmat(x(N(j), span()), p, 1);
        U_j = repmat(u(N(j), span()), p, 1);

        // euclidean norm between i and j
        const float d_ij = norm(x(id, span(0, 1)) - x(N(j), span(0, 1)));

        // calculate the desired position to x and y coordinates
        const rowvec dxy = (x(id, span(0, 1)) - x(N(j), span(0, 1)))*(r_cov(id) + r_cov(N(j)))/d_ij + x(N(j), span(0, 1));

        // calculate the desired value to Θ angle
        const rowvec dtheta = ones(1)*0;

        // auxiliary matrix to desired position
        X_d = repmat(join_horiz(dxy, dtheta), p, 1);

        std::cout << "x_d[" << id << "] -->>" << X_d(0, span()) << std::endl;

        // mpc term activation
        const int phi_ij = (1 - A(N(j)))*H(N(j));
        const int psi_ij = A(N(j)) + phi_ij;

        // fill auxiliary matrices
        Hxy += T.t()*dt*dt*psi_ij*gamma(0)/r_com(j)*T + phi_ij*gamma(1)/ux_max + (psi_ij - H(N(j)))*gamma(2)/ux_max;
        Htheta += T.t()*dt*dt*psi_ij*gamma(0)/90*T;
        gxy += (X_i(span(), span(0, 1)) - X_d(span(), span(0, 1))).t()*dt*psi_ij*gamma(0)/r_com(j)*T + U_j(span(), span(0, 1)).t()*phi_ij*gamma(1)/ux_max - U_j(span(), span(0, 1)).t()*(psi_ij - H(N(j)))*gamma(2)/ux_max;
        gtheta += (X_i.col(2) - X_d.col(2)).t()*dt*psi_ij*gamma(0)/90*T;
    }

    // fill auxiliary matrices
    Hxy += TI.t()*gamma(3)/ux_max*TI;
    Htheta += TI.t()*gamma(3)/ua_max*TI;
    gxy -= U_i0(span(), span(0, 1)).t()*gamma(3)/ux_max*TI;
    gtheta -= U_i0.col(2).t()*gamma(3)/ua_max*TI;

    // fill main matrices with the processed ones
    for(unsigned int i = 0; i < p; i++) {
        for(unsigned int j = i; j < p; j++) {
            G[i][j] = Hxy(i, j);
            G[j][i] = Hxy(j, i);
            G[i+p][j+p] = Hxy(i, j);
            G[j+p][i+p] = Hxy(j, i);
            G[i+p*2][j+p*2] = Htheta(i, j);
            G[j+p*2][i+p*2] = Htheta(j, i);
        }

        g0[i] = gxy(0, i);
        g0[i+p] = gxy(1, i);
        g0[i+p*2] = gtheta(i);
    }

    // quadprog solver
    double J = solve_quadprog(G, g0, CE, ce0, CI, ci0, U);

    // verify the problem feasibility
    if(std::isinf(J) || std::isnan(J)) {
        U = 0;
        cout << "ERROR: Not feasible problem!" << endl;
    }

    // control output
    vec out = zeros<vec>(dim);

    // get 1st control signals to each dimension
    out(0) = U[0];
    out(1) = U[p];
    out(2) = U[p*2];

    // translate control signals from inertial to local frame
    out(span(0, 1)) = rot(out(span(0, 1)), x(id, 2)*PI/180);

    // verify control signal saturations (after the frame translation, the values
    // can violate the control signal limits)
    out(0) > ux_max ? out(0) = ux_max : out(0) < ux_min ? out(0) = ux_min : 0;
    out(1) > ux_max ? out(1) = ux_max : out(1) < ux_min ? out(1) = ux_min : 0;

    return out;
}

/* second order MPC motion controller */
vec control::mpc_2nd_order(mat x, mat v, mat u, uvec A, uvec H) {
    // dimensions of the control array
    const unsigned int dim = 3;

    // number of elements in X (we have p predictions for each dimension)
    const unsigned int n = p*dim;

    // number of elements in ci0 (2 = we have ub and lb for all predictions)
    const unsigned int m = 2*n;

    // get the real and virtual neighbours of id
    const uvec N = find(max(A, H));

    /*-------------------------------------------------------------------------
    Quadratic optimization form:
    min 0.5 * x G x + g0 x
    s.t.
        CE^T x + ce0 = 0
        CI^T x + ci0 >= 0

    The matrix and vectors dimensions are as follows:
        G: n * n
        g0: n

        CE: n * p
        ce0: p

        CI: n * m
        ci0: m

        x: n
    -------------------------------------------------------------------------*/

    // main matrices of quadratic formulation (using quadprog++ matrix type)
    Matrix<double> G(n, n), CE, CI(n, m);
    Vector<double> g0(n), U(n), ce0, ci0(m);

    // temporary matrices to make formulation
    mat Hxy = zeros(p, p);              // hessian matrix to x and y coordinates
    mat Htheta = zeros(p, p);           // hessian matrix to θ angle (YAW)
    rowvec gx = zeros<rowvec>(p);       // gradient array to x coordinate
    rowvec gy = zeros<rowvec>(p);       // gradient array to y coordinate
    rowvec gtheta = zeros<rowvec>(p);   // gradient array to θ angle (YAW)

    // position, velocity, and control arrays
    mat U_i0 = zeros(p, dim);
    mat U_j = zeros(p, dim);
    mat V_j = zeros(p, dim);
    mat V_i = zeros(p, dim);
    mat X_i = zeros(p, dim);
    mat X_j = zeros(p, dim);
    mat X_d = zeros(p, dim);

    // zero fill
    G = 0;
    U = 0;
    CI = 0;
    ci0 = 0;

    // fill control variable
    U_i0(0, span()) = u(id, span());

    // fill velocity array
    V_i = repmat(v(id, span()), p, 1);

    // fill state array
    X_i = repmat(x(id, span()), p, 1);

    // velocity variation limit
    const float v_max = ux_max*dt;

    // build the control signal saturation constraints
    for(unsigned int i = 0; i < p; i++) {
        CI[i][i] = -1;              // -ux + ux_max >= 0
        CI[i][i+p] = 1;             //  ux - ux_min >= 0
        CI[i+p][i+p*2] = -1;        // -uy + uy_max >= 0
        CI[i+p][i+p*3] = 1;         //  uy - uy_min >= 0
        CI[i+p*2][i+p*4] = -1;      // -uθ + uθ_max >= 0
        CI[i+p*2][i+p*5] = 1;       //  uθ - uθ_min >= 0

        ci0[i] = ux_max;            //  ux_max
        ci0[i+p] = -ux_min;         // -ux_min
        ci0[i+p*2] = ux_max;        //  uy_max
        ci0[i+p*3] = -ux_min;       // -uy_min
        ci0[i+p*4] = ua_max;        //  uθ_max
        ci0[i+p*5] = -ua_min;       // -uθ_min
    }

    // build the control matrices for each neighbor of i
    for(unsigned int j = 0; j < N.n_elem; j++) {
        // fill neighbor state and control arrays
        X_j = repmat(x(N(j), span()), p, 1);
        V_j = repmat(x(N(j), span()), p, 1);
        U_j = repmat(u(N(j), span()), p, 1);

        // euclidean norm between i and j
        const float d_ij = norm(x(id, span(0, 1)) - x(N(j), span(0, 1)));

        // calculate the desired position to x and y coordinates
        const rowvec dxy = (x(id, span(0, 1)) - x(N(j), span(0, 1)))*(r_cov(id) + r_cov(N(j)))/d_ij + x(N(j), span(0, 1));

        // calculate the desired value to Θ angle
        const rowvec dtheta = ones(1)*0;

        // auxiliary matrix to desired position
        X_d = repmat(join_horiz(dxy, dtheta), p, 1);

        std::cout << "x_d[" << id << "] -->>" << X_d(0, span()) << std::endl;

        // mpc term activation
        const int phi_ij = (1 - A(N(j)))*H(N(j));   // activated for virtual neighbors
        const int psi_ij = A(N(j)) + phi_ij;        // activated for all neighbors (physical + virtual)

        // fill auxiliary matrices
        Hxy += T2.t()*dt*dt*dt*dt*gamma(0)/r_com(N(j))*psi_ij*T2;
        Hxy += T.t()*dt*dt*gamma(1)/2*v_max*phi_ij*T;
        Hxy += T.t()*dt*dt*gamma(2)/v_max*(psi_ij - H(N(j)))*T;
        Htheta += T2.t()*dt*dt*dt*dt*gamma(0)*psi_ij*T2;

        gx += (X_i.col(0) - X_d.col(0)).t()*dt*dt*gamma(0)/r_com(N(j))*psi_ij*T2;
        gy += (X_i.col(1) - X_d.col(1)).t()*dt*dt*gamma(0)/r_com(N(j))*psi_ij*T2;
        gtheta += (X_i.col(2) - X_d.col(2)).t()*dt*dt*psi_ij*gamma(0)*T2;
        gx += V_i.col(0).t()*P.t()*dt*dt*dt*gamma(0)/r_com(N(j))*psi_ij*T2;
        gy += V_i.col(1).t()*P.t()*dt*dt*dt*gamma(0)/r_com(N(j))*psi_ij*T2;
        gtheta += V_i.col(2).t()*P.t()*dt*dt*dt*gamma(0)*psi_ij*T2;
        gx += (V_i.col(0) + V_j.col(0)).t()*dt*gamma(1)/2*v_max*phi_ij*T;
        gy += (V_i.col(1) + V_j.col(1)).t()*dt*gamma(1)/2*v_max*phi_ij*T;
        gx += (V_i.col(0) - V_j.col(0)).t()*dt*gamma(2)/v_max*(psi_ij - H(N(j)))*T;
        gy += (V_i.col(1) - V_j.col(1)).t()*dt*gamma(2)/v_max*(psi_ij - H(N(j)))*T;
    }

    // fill auxiliary matrices
    Hxy += TI.t()*gamma(3)*TI;
    Htheta += TI.t()*gamma(3)*TI;
    gx -= U_i0.col(0).t()*gamma(3)/ux_max*TI;
    gy -= U_i0.col(1).t()*gamma(3)/ux_max*TI;
    gtheta -= U_i0.col(2).t()*gamma(3)/ua_max*TI;

    // fill main matrices with the processed ones
    for(unsigned int i = 0; i < p; i++) {
        for(unsigned int j = i; j < p; j++) {
            G[i][j] = Hxy(i, j);
            G[j][i] = Hxy(j, i);
            G[i+p][j+p] = Hxy(i, j);
            G[j+p][i+p] = Hxy(j, i);
            G[i+p*2][j+p*2] = Htheta(i, j);
            G[j+p*2][i+p*2] = Htheta(j, i);
        }

        g0[i] = gx(i);
        g0[i+p] = gy(i);
        g0[i+p*2] = gtheta(i);
    }

    // quadprog solver
    double J = solve_quadprog(G, g0, CE, ce0, CI, ci0, U);

    // verify the problem feasibility
    if(std::isinf(J) || std::isnan(J)) {
        U = 0;
        cout << "ERROR: Not feasible problem!" << endl;
    }

    // control output
    vec out = zeros<vec>(dim);

    // get 1st control signals to each dimension
    out(0) = U[0];
    out(1) = U[p];
    out(2) = U[p*2];

    // translate control signals from inertial to local frame
    //out(span(0, 1)) = rot(out(span(0, 1)), x(id, 2)*PI/180);

    // verify control signal saturations (after the frame translation, the values
    // can violate the control signal limits)
    /*out(0) > ux_max ? out(0) = ux_max : out(0) < ux_min ? out(0) = ux_min : 0;*/
    /*out(1) > ux_max ? out(1) = ux_max : out(1) < ux_min ? out(1) = ux_min : 0;*/

    return out;
}

/* simple proportional control */
vec control::p_motion_control(vec x, vec r, vec u) {
    // proportionality constant
    const float kx = 1.0;
    const float ka = 1.0;

    // minimum error
    const float ex_min = 0.01;
    const float ea_min = 0.01;

    // maximum error
    const float ex_max = 50.0;
    const float ea_max = PI;

    // convert from degrees to radians
    x(2) *= PI/180;
    r(2) *= PI/180;

    // normalize the angles between 0 and π radians
    x(2) > PI + 1 ? x(2) -= 2*PI : 0;
    r(2) > PI + 1 ? r(2) -= 2*PI : 0;

    // error between current and the reference point
    const float ex = (x(0) - r(0))/ex_max;
    const float ey = (x(1) - r(1))/ex_max;
    const float ea = (x(2) - r(2))/ea_max;

    printf("x0:%.2f  xr:%.2f  ex:%.2f  dx:%.2fm\n", x(0), r(0), ex, fabs(x(0) - r(0)));
    printf("y0:%.2f  yr:%.2f  ey:%.2f  dy:%.2fm\n", x(1), r(1), ey, fabs(x(1) - r(1)));
    printf("θ0:%.2f  θr:%.2f  eθ:%.2f  dθ:%.2frad\n", x(2), r(2), ea, fabs(x(2) - r(2)));

    // calculate the new control signal
    fabs(ea) < ea_min && fabs(ex) > ex_min ? u(0) = kx*ex : u(0) = 0;
    fabs(ea) < ea_min && fabs(ey) > ex_min ? u(1) = kx*ey : u(1) = 0;
    fabs(ea) > ea_min ? u(2) = ka*ea : u(2) = 0;

    // translate control signals from inertial to local frame
    u(span(0, 1)) = rot(u(span(0, 1)), x(2));

    // verify control signal saturations
    u(0) > ux_max ? u(0) = ux_max : u(0) < ux_min ? u(0) = ux_min : 0;
    u(1) > ux_max ? u(1) = ux_max : u(1) < ux_min ? u(1) = ux_min : 0;
    u(2) > ua_max ? u(2) = ua_max : u(2) < ua_min ? u(2) = ua_min : 0;

    return u;
}

/* translate coordinates between different frames */
vec control::rot(arma::vec x, float yaw) {
    vec trl_x = zeros(x.n_elem);

    // translate coordinates from a reference to another
    // (it uses a clockwise rotation matrix)
    trl_x(0) = x(0)*cos(yaw) + x(1)*sin(yaw);
    trl_x(1) = - x(0)*sin(yaw) + x(1)*cos(yaw);

    return trl_x;
}

