/******************************************************************************
 * Network Module for Topology Control Algorithm <Implementation>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2016 Nov 24 17:20:13
 * Info: This file contains the source code of the network communication
 * algorithms
 *****************************************************************************/

#include <network.hpp>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

/* return an array of bytes from any data input */
unsigned char *get_raw(const void *data, size_t data_len) {
    unsigned char *raw = new unsigned char[data_len];

    // fill output array with bytes from input
    for(size_t i = 0; i < data_len; i++) raw[i] = ((const unsigned char *) data)[i];

    return raw;
}

/* print raw bytes from a input data */
void print_raw(const void *data, size_t data_len) {
    printf("\n\tRAW DATA -> [0x");
    for(size_t i = 0; i < data_len; i++) {
        printf("%02X", ((const unsigned char *) data)[i]);
        i < data_len - 1 ? printf(":") : 0;
    }
    printf("]\n\n");
}

/* convert a state_t variable to a byte array */
unsigned char *serialize(state_t data) {
    unsigned char *raw = new unsigned char[data.n_bytes];

    memcpy(&raw[0], get_raw(&data.n_bytes, 2), 2);
    memcpy(&raw[2], get_raw(&data.id, 1), 1);
    memcpy(&raw[3], get_raw(&data.n1_size, 1), 1);
    memcpy(&raw[4], get_raw(data.n1, data.n1_size), data.n1_size);
    memcpy(&raw[4 + data.n1_size], get_raw(data.x, sizeof(pose_t)*(data.n1_size + 1)), sizeof(pose_t)*(data.n1_size + 1));
    memcpy(&raw[4 + data.n1_size + sizeof(pose_t)*(data.n1_size + 1)], get_raw(data.u, sizeof(pose_t)*(data.n1_size + 1)), sizeof(pose_t)*(data.n1_size + 1));
    memcpy(&raw[4 + data.n1_size + sizeof(pose_t)*(data.n1_size + 1)*2], get_raw(data.r_cov, sizeof(float)*(data.n1_size + 1)), sizeof(float)*(data.n1_size + 1));
    memcpy(&raw[4 + data.n1_size + sizeof(pose_t)*(data.n1_size + 1)*2 + sizeof(float)*(data.n1_size + 1)], get_raw(data.r_com, sizeof(float)*(data.n1_size + 1)), sizeof(float)*(data.n1_size + 1));

    return raw;
}

/* convert a byte array into a state_t variable */
state_t deserialize(unsigned char *raw_data) {
    state_t data;

    memcpy(&data, raw_data, 4);

    // allocate memory space to c pointers
    data.n1 = new uint8_t[data.n1_size];
    data.x = new pose_t[data.n1_size + 1];
    data.u = new pose_t[data.n1_size + 1];
    data.r_cov = new float[data.n1_size + 1];
    data.r_com = new float[data.n1_size + 1];

    // copy data from byte_array to each field of state_t
    memcpy(data.n1, (unsigned char *) raw_data + 4, data.n1_size);
    memcpy(data.x, (unsigned char *) raw_data + 4 + data.n1_size, sizeof(pose_t)*(data.n1_size + 1));
    memcpy(data.u, (unsigned char *) raw_data + 4 + data.n1_size + sizeof(pose_t)*(data.n1_size + 1), sizeof(pose_t)*(data.n1_size + 1));
    memcpy(data.r_cov, (unsigned char *) raw_data + 4 + data.n1_size + 2*sizeof(pose_t)*(data.n1_size + 1), sizeof(float)*(data.n1_size + 1));
    memcpy(data.r_com, (unsigned char *) raw_data + 4 + data.n1_size + 2*sizeof(pose_t)*(data.n1_size + 1) + sizeof(float)*(data.n1_size + 1), sizeof(float)*(data.n1_size + 1));

    return data;
}

/* RX thread constructor */
RXThread::RXThread(unsigned int port) {
    // our address
    struct sockaddr_in src_addr;

    // initialize receiver UDP socket
    udpr = socket(AF_INET, SOCK_DGRAM, 0);

    // check if the socket was opened
    if(udpr < 0) {
        std::cout << "ERROR: cannot create receiver socket!" << std::endl;
        return;
    }

    // associate data with address struct
    memset((char *)&src_addr, 0, sizeof(src_addr));       // set all fields of address as null
    src_addr.sin_family = AF_INET;                        // address format (IPv4)
    src_addr.sin_addr.s_addr = htonl(INADDR_ANY);         // convert local ip to network format
    src_addr.sin_port = htons(port);                      // convert port to network format

    // link the address (ip + port) with the socket
    // check if the socket was corrected associated with the address
    if(bind(udpr, (struct sockaddr *)&src_addr, sizeof(src_addr)) < 0) {
        std::cout << "ERROR: bind to receiver socket failed!" << std::endl;
        return;
    }

    // set new message indicator as false
    new_msg = false;

    // set thread run indicator as true
    run = true;

    // set lockers as false
    get_msg_lock = rec_msg_lock = false;

    // start receiver thread
    rx_thread = new std::thread(&RXThread::rx_loop, this);
}

/* RX thread destructor */
RXThread::~RXThread() {
    // stop RX thread
    stop();
}

/* RX loop */
void RXThread::rx_loop() {
    struct sockaddr_in remaddr;                 // remote address
    socklen_t addrlen = sizeof(remaddr);        // length of addresses
    unsigned char msg[MSG_SIZE];                // message buffer
    unsigned char *tmp;                         // temporary memory to store the received message
    unsigned int recvlen;                       // bytes received

    // receiver loop
    while(run) {
        // get the message from UDP socket
        recvlen = recvfrom(udpr, msg, MSG_SIZE, 0, (struct sockaddr *)&remaddr, &addrlen);

        // set new message indicator as true
        new_msg = true;

        // wait get message locker be free
        while(get_msg_lock);

        // set receive message locker as true
        rec_msg_lock = true;

        // verify the size of received message and if the current message is
        // equal to the last one
        if(recvlen > 0 && (msg_pool.empty() || (!msg_pool.empty() && strcmp((const char *) msg, (const char *) msg_pool.back()) != 0))) {
            // allocate a new memory space to temporary variable
            tmp = (unsigned char *) malloc(recvlen);

            // copy the received message to temporary memory space
            memcpy(tmp, msg, recvlen);

            // add received message to the end of message pool
            msg_pool.push_back(tmp);

            // add message length to the end of message length array
            msg_len.push_back(recvlen);

            //printf("pool_size:%d\n", msg_pool.size());
        }

        // set received message locker as false
        rec_msg_lock = false;
    }
}

/* get received message pool */
void RXThread::get_msg(float timeout, std::vector<void *> &msg_pool, std::vector<unsigned int> &msg_len) {
    // waiting time
    float time = 0;

    // wait for a new message
    while(!new_msg && run) {
        // wait a time
        sleep(1);

        // increase the waiting time
        time += 1;

        // if the waiting time is bigger than timeout, exit
        if(time > timeout) return;
    }

    // wait receive message locker
    while(rec_msg_lock);

    // set new message indicator as false
    new_msg = false;

    // lock message buffer semaphore
    get_msg_lock = true;

    // copy message pool to outside
    msg_pool = this->msg_pool;

    // copy message length array to outside
    msg_len = this->msg_len;

    // clean message buffer
    this->msg_pool.clear();

    // clean message length array
    this->msg_len.clear();

    // unlock message buffer semaphore
    get_msg_lock = false;
}

/* stop RX thread */
void RXThread::stop() {
    // set run indicator as false
    run = false;

    // wait for the thread response
    rx_thread->join();

    // close receiver socket
    close(udpr);
}

/* TX thread constructor */
TXThread::TXThread(float dt, unsigned int src_port, std::vector<const char *> dst_ip, std::vector<short unsigned int> dst_port) {
    // our address
    struct sockaddr_in src_addr;

    // initialize receiver UDP socket
    udps = socket(AF_INET, SOCK_DGRAM, 0);

    // check if the socket was opened
    if(udps < 0) {
        std::cout << "ERROR: cannot create sender socket!" << std::endl;
        return;
    }

    // associate data with address struct
    memset((char *)&src_addr, 0, sizeof(src_addr));       // set all fields of address as null
    src_addr.sin_family = AF_INET;                        // address format (IPv4)
    src_addr.sin_addr.s_addr = htonl(INADDR_ANY);         // convert local ip to network format
    src_addr.sin_port = htons(src_port);                  // convert port to network format

    // link the address (ip + port) with the socket
    // check if the socket was corrected associated with the address
    if(bind(udps, (struct sockaddr *)&src_addr, sizeof(src_addr)) < 0) {
        std::cout << "ERROR: bind to sender socket failed!" << std::endl;
        return;
    }

    // set thread run indicator as true
    run = true;

    // nullify the first message
    msg = "";

    // start sender thread
    tx_thread = new std::thread(&TXThread::tx_loop, this, dt, dst_ip, dst_port);
}

/* TX thread destruct */
TXThread::~TXThread() {
    // stop TX thread
    stop();
}

/* TX loop */
void TXThread::tx_loop(float dt, std::vector<const char *> dst_ip, std::vector<short unsigned int> dst_port) {
    // destination address
    struct sockaddr_in dst_addr;

    // sender loop
    while(run) {
        // send the message for all ports
        for(unsigned int p = 0; p < dst_port.size(); p++) {
            // fill the destination address
            memset((char *)&dst_addr, 0, sizeof(dst_addr));     // set all fields of address as null
            dst_addr.sin_family = AF_INET;                      // address format (IPv4)
            dst_addr.sin_addr.s_addr = inet_addr(dst_ip[p]);    // convert local ip to network format
            dst_addr.sin_port = htons(dst_port[p]);             // convert port to network format

            // send the message to dst_port + dst_ip
            sendto(udps, msg, msg_len, 0, (struct sockaddr *)&dst_addr, sizeof(dst_addr));

            // wait a time between transmissions
            usleep(dt*1e6);
        }
    }
}

/* send a message */
void TXThread::send_msg(const void *msg, unsigned int msg_len) {
    this->msg = msg;
    this->msg_len = msg_len;
}

/* stop TX thread */
void TXThread::stop() {
    // set run indicator as false
    run = false;

    // wait for the thread response
    tx_thread->join();

    // close receiver socket
    close(udps);
}

/* message history constructor */
MHistory::MHistory(unsigned int nodes_num) {
    // set nodes number with the parameter
    this->nodes_num = nodes_num;

    // initialize the history array
    hist = arma::zeros<arma::umat>(nodes_num, HIST_WIN_SIZE);

    // initialize the time slot
    t_slot = 0;
}

/* message history destructor */
MHistory::~MHistory() {
    // NOP
}

/* mark received message to a node */
void MHistory::mark_msg(unsigned int id) {
    hist(id, t_slot) = true;

    //std::cout << "hist:" << hist(arma::span(), arma::span()) << std::endl;
}

/* advance with the current time slot */
void MHistory::inc_time() {
    t_slot < HIST_WIN_SIZE - 1 ? t_slot += 1 : t_slot = 0;

    // set as false all history for the new time slot
    hist(arma::span(), t_slot) = arma::zeros<arma::umat>(nodes_num);
}

