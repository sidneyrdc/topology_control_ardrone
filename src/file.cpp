/******************************************************************************
 * External Files Tools <Implementation>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2017 Out 10 20:14:07
 * Info: This file contains the header of the functions to manipulate external
 * archive files, as text and matlab files.
 *****************************************************************************/

#include <file.hpp>

using namespace arma;
using namespace std;

/* class constructor */
DataFile::DataFile(string file_name, int file_type) {
    // set output file name and type
    strcpy(this->file_name, file_name.c_str());
    this->file_type = file_type;

    // open data file
    open();
}

/* class destructor */
DataFile::~DataFile() {
    // close data file
    close();
}

/* write data in the archive file */
void DataFile::write(int step, rowvec x, rowvec v, rowvec u, float r_com, float r_cov, float dt) {
    // check the file type
    switch(file_type) {
        case CSV:
            // write data in the output file
            fprintf(data_file, "%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", step, x(0),
                    x(1), x(2), v(0), v(1), v(2), u(0), u(1), u(2), r_com, r_cov, dt);

            // commit the data to the archive file
            fflush(data_file);
        default:
            ;
    }
}

/* open data file */
void DataFile::open() {
    // check the file type
    switch(file_type) {
        case CSV:
            // open new data file to write the data
            data_file = fopen(strcat(file_name, ".csv"), "w");

            // write the csv file header
            fprintf(data_file, "step,pose_x,pose_y,pose_yaw,vel_x,vel_y,vel_yaw,ctrl_x,ctrl_y,ctrl_yaw,r_com,r_cov,dt\n");
        default:
            ;
    }

}

/* close data file */
void DataFile::close() {
    fclose(data_file);
}

