/******************************************************************************
 * Topology Control Algorithm - AR.Drone version <Main>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2017 Out 11 19:05:30
 * Info: This file is the main file to the Topology Control Algorithm
 *****************************************************************************/

#include <control.hpp>
#include <network.hpp>
#include <stdlib.h>
#include <utils.hpp>
#include <file.hpp>
#include <signal.h>

extern "C" {
#include <drk/drk.h>                    // drk_init()
#include <drk/flight_control_api.h>     // translate
#include <drk/gps.h>                    // read gps samples
#include <drk/internal_sensor_raw.h>    // heading , etc
}

using namespace arma;

int main(int argc, char **argv) {
    // verify if the parameters are correct
    if(argc < 2) {
        cout << "Usage: " << argv[0] << " <id> <nodes_number>" << endl;
        exit(1);
    } else if(atoi(argv[1]) < 1) {
        cout << "@ERROR: id must be greater than zero!" << endl;
        exit(1);
    }

    // initial configuration
    const unsigned int real_id = atoi(argv[1]);
    const unsigned int id = real_id - 1;   // rk_drone lib does accept indexes lower than 1
    const unsigned int n = 4;
    const unsigned int p = 3;
    const float dt = 3;
    const float ux_max = 1;
    const float ux_min = -1;
    const float ua_max = 0.2;
    const float ua_min = -0.2;

    // iteration counter
    unsigned int iter_count = 0;

    // control parameters matrices
    vec r_com = ones(n)*55;                 // communication radius array
    vec r_cov = ones(n)*25;                 // coverage radius array
    vec gamma = ones(4);                    // mpc weight variables
    gamma(0) = 5;
    gamma(1) = 0;
    gamma(2) = 0;
    gamma(3)= 0.1;

    // state matrix (x, y, θ)
    mat x = zeros(n, 3);

    // velocity matrix (vx, vy, vθ)
    mat v = zeros(n, 3); mat v_ = zeros(n, 3);

    // control matrix (ux, uy, uθ)
    mat u = zeros(n, 3); vec u_ = zeros(3);

    // adjacency array
    // A(0, :) -> 1-hop adjacency array
    // A(1, :) -> 2-hop adjacency array
    umat A = zeros<umat>(2, n);

    // Hamiltonian array (TSP's solution)
    uvec H = zeros<uvec>(n);

    // state message
    state_t x_msg;

    // control message
    pose_t u_msg;

    // received message pool and length
    std::vector<void *> msg_pool;
    std::vector<unsigned int> msg_len;

    // message history array
    MHistory msg_hist = MHistory(n);

    // 1-hop neighbors set
    uvec N1; uvec N1_;

    // 1-hop + 2-hop neighbors set
    uvec N2; uvec N2_;

    // destination ports and ips
    std::vector<const char *> dst_ip(n - 1);
    std::vector<short unsigned int> dst_port(n - 1);

    // fill the destination port and ip arrays
    unsigned int j = 0;
    for(unsigned int i = 1; i <= n; i++) {
        if(i != real_id) {
            dst_ip[j] = "127.0.0.1";
            dst_port[j] = UDP_PORT_BASE_RX_ALL + i;
            j++;
        }
    }

    // start AR.Drone interface
    if(drk_init(real_id) < 0) return 1;

    // ports and ips to actuator
    //std::vector<const char *> act_ip; act_ip.push_back("127.0.0.1");
    //std::vector<short unsigned int> act_port; act_port.push_back(UDP_PORT_BASE_RX_ALL + real_id);

    // transmission threads
    //TXThread tx_actuators = TXThread(dt, UDP_PORT_BASE_SRC_ACT + real_id, act_ip, act_port);
    TXThread tx_wifi = TXThread(1, UDP_PORT_BASE_SRC_80211 + real_id, dst_ip, dst_port);

    // reception threads
    //RXThread rx_sensors = RXThread(UDP_PORT_BASE_TX_SENSOR + real_id);
    RXThread rx_wifi = RXThread(UDP_PORT_BASE_TX_80211 + real_id);

    // connectivity motion control object
    control cmc = control(id, p, dt, ux_max, ux_min, ua_max, ua_min, r_com, r_cov, gamma);

    // file name with 'data-id'
    std::stringstream sstm;
    sstm << "./log/data-" << real_id;

    // output data file
    DataFile output = DataFile(sstm.str(), CSV);

    // main loop
    while(1) {

        // get sensors information
        u_msg = lla2xyz(drk_gps_data());        // convert coordinates from gps to local frame
        x(id, 0) = u_msg.x;                     // X-axis coordinate
        x(id, 1) = u_msg.y;                     // Y-axis coordinate
        x(id, 2) = comp2cart(drk_heading());    // YAW angle in degrees (heading)

        /*
         * Communication Stuff (COM)
         */

        if(iter_count % HIST_WIN_SIZE == 0) {
            // clean the 2-hop adjacency matrix data
            A(1, span()) = zeros<umat>(1, n);

            // reset iteration number
            //iter_count = 0;
        }

        printf("RCOM -> %f", r_com(id));

        // save data file
        output.write(iter_count, x(id, span()), v(id, span()), u(id, span()), r_com(id), r_cov(id), dt);

        // clean message structs
        memset(&u_msg, 0, sizeof(pose_t));
        memset(&x_msg, 0, sizeof(state_t));

        // fill control message with control values
        u_msg.x = u(id, 0);
        u_msg.y = u(id, 1);
        u_msg.z = 0;
        u_msg.yaw = u(id, 2);

        // send control signals
        //tx_actuators.send_msg(&u_msg, sizeof(u_msg));
        drk_translate(-u_msg.x, -u_msg.y, u_msg.yaw, 0, 0);   // (pitch, roll, yaw, gaz, 0)

        // fill state message
        x_msg.id = real_id;
        x_msg.n1_size = N1.n_elem;
        x_msg.n_bytes = 2 + x_msg.n1_size + ((x_msg.n1_size + 1)*sizeof(pose_t))*2 + ((x_msg.n1_size + 1)*sizeof(float))*2;

        x_msg.n1 = new uint8_t[x_msg.n1_size];
        x_msg.x = new pose_t[x_msg.n1_size + 1];
        x_msg.u = new pose_t[x_msg.n1_size + 1];
        x_msg.r_cov = new float[x_msg.n1_size + 1];
        x_msg.r_com = new float[x_msg.n1_size + 1];

        x_msg.x[0].x = x(id, 0);
        x_msg.x[0].y = x(id, 1);
        x_msg.x[0].z = 0;
        x_msg.x[0].yaw = x(id, 2);

        x_msg.u[0] = u_msg;

        x_msg.r_cov[0] = r_cov(id);
        x_msg.r_com[0] = r_com(id);

        for(uint8_t i = 1; i < x_msg.n1_size + 1; i++) {
            x_msg.n1[i - 1] = N1(i - 1);

            x_msg.x[i].x = x(N1(i - 1), 0);
            x_msg.x[i].y = x(N1(i - 1), 1);
            x_msg.x[i].z = 0;
            x_msg.x[i].yaw = x(N1(i - 1), 2);

            x_msg.u[i].x = u(N1(i - 1), 0);
            x_msg.u[i].y = u(N1(i - 1), 1);
            x_msg.u[i].z = 0;
            x_msg.u[i].yaw = u(N1(i - 1), 2);

            x_msg.r_cov[i] = r_cov(N1(i - 1));
            x_msg.r_com[i] = r_com(N1(i - 1));
        }

        printf("\nSENDER DATA --> id:%d msg_size:%d\n", x_msg.id, x_msg.n_bytes);

        // send state message (broadcast mode)
        tx_wifi.send_msg((unsigned char *) serialize(x_msg), x_msg.n_bytes);

        // clear message pool and lengths
        msg_pool.clear();
        msg_len.clear();

        // get message from neighbors
        rx_wifi.get_msg(1, msg_pool, msg_len);

        // analyze and extract data from received WiFi messages
        for(unsigned int i = 0; i < msg_pool.size(); i++) {
            // clear state message struct
            memset(&x_msg, 0, sizeof(state_t));

            // get the received message
            x_msg = deserialize((unsigned char *) msg_pool[i]);

            printf("\nRECEIVER DATA --> id:%d sender_id:%d msg_size:%d\n\n", real_id, x_msg.id, x_msg.n_bytes);

            // sender id
            j = x_msg.id - 1;

            // check if the sender is itself
            if(j == id) continue;

            // set the message sender as 1-hop neighbor in the adjacency matrix
            A(0, j) = 1;

            // remove sender from 2-hop neighbor adjacency array
            A(1, j) = 0;

            // mark message history as received to sender id
            msg_hist.mark_msg(j);

            // get sender's state, control, converage and communication radius
            x(j, 0) = x_msg.x[0].x;
            x(j, 1) = x_msg.x[0].y;
            x(j, 2) = x_msg.x[0].yaw;

            u(j, 0) = x_msg.u[0].x;
            u(j, 1) = x_msg.u[0].y;
            u(j, 2) = x_msg.u[0].yaw;

            v(j, span()) = v(j, span()) + u(j, span())*dt;

            r_cov(j) = x_msg.r_cov[0];
            r_com(j) = x_msg.r_com[0];

            for(j = 0; j < x_msg.n1_size; j++) {
                if(x_msg.n1[j] != id) {
                    // get 2-hop neighbors' state, control, coverage and communication radius
                    x(j, 0) = x_msg.x[j + 1].x;
                    x(j, 1) = x_msg.x[j + 1].y;
                    x(j, 2) = x_msg.x[j + 1].yaw;

                    u(j, 0) = x_msg.u[j + 1].x;
                    u(j, 1) = x_msg.u[j + 1].y;
                    u(j, 2) = x_msg.u[j + 1].yaw;

                    v(j, span()) = v(j, span()) + u(j, span())*dt;

                    r_cov(j) = x_msg.r_cov[j + 1];
                    r_com(j) = x_msg.r_com[j + 1];

                    // set 2-hop neighbors
                    A(1, x_msg.n1[j]) = !A(0, x_msg.n1[j]);
                }
            }
        }

        // remove mute nodes from 1-hop neighborhood
        for(unsigned int i = 0; i < n; i++) {
            if(A(0, i) != 0 && sum(msg_hist.hist(i, span())) == 0) A(0, i) = 0;
        }

        // advance the time slot on history array
        msg_hist.inc_time();

        // update neighbors set
        N1 = find(A(0, span()));
        N2 = find(A(1, span()));

        cout << "N1:" << (1 + N1.t()) << "\nN2:" << (1 + N2.t()) << endl;
        cout << "x:\n" << x << endl << endl;
        cout << "u:\n" << u << endl << endl;

        /*
         * Control Stuff (TSP + MPC)
         */

        // gets Hamiltonian cycle from TSP
        H = zeros<uvec>(n);

        // save current control signal
        u_ = u(id, span()).t();

        // solves the MPC to motion control
        u(id, span()) = cmc.mpc_2nd_order(x, v, u, A.row(0).t(), H).t();
        //u(id, span()) = -cmc.mpc_1st_order(x, u, A.row(0).t(), H).t();
        u(id, 2) = 0;

        // update velocities
        v(id, span()) = v(id, span()) + u(id, span())*dt;

        //vec xr;
        //xr << 50 << -50 << 270 << endr;
        /*u(id, span()) = cmc.p_motion_control(x(id, span()).t(), xr, u(id, span()).t()).t();*/

        // control signal compensation to inertial movement
/*        fabs(u(id, 0)) < 0.1 ? u(id, 0) = -u_(0) : 0;*/
        /*fabs(u(id, 1)) < 0.1 ? u(id, 1) = -u_(1) : 0;*/
        //fabs(u(id, 2)) < 0.001 ? u(id, 2) = -u_(2) : 0;

        // increase iteration number
        iter_count++;
    }

    return 0;
}

