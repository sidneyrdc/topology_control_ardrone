%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read Data Function
%
% Maintainer: Sidney Carvalho - sydney.rdc@gmail.com
% Last Change: 2017 Out 11 19:02:52
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [N, n, x, v, u, r_com, r_cov, dt] = read_data(data_prefix)
%function [x] = read_data(data_prefix)
% help
% draw_robot() draw a triangle robot shape centered in a point x
%
% draw_robot(x, theta, length) draw the robot with default parameters
% draw_robot(x, theta, length, RobotShape, RobotLabel, LineColor...) draw the
% robot with extra parameters
%
% Input arguments:
% ----------------------
% x: Coordinates of the robot's center point (meters)
% theta: Robot orientation (radians)
% length: Robot length (meters)
%
% Optional parameters (passed as parameter/value pairs):
% ------------------------------------------------------
%
% 'RobotShape'      :   Shape of the robot
%                       default: 't' (triangular)
% 'RobotLabel'      :   Name to be printed into the robot's shape
%                       default: ''
% 'FillColor'       :   Colour of the shape (vector of colour or char)
%                       default: 'w'
%
% Example:
% --------
% draw_robot([0 0], 0, ...
%           'RobotLabel', 'robot', ...
%           'FillColor', 'r')
%

% matlab output arrays
x_temp = zeros(100, 3, 10000);
v_temp = zeros(100, 3, 10000);
u_temp = zeros(100, 3, 10000);
r_cov_temp = zeros(100, 10000);
r_com_temp = zeros(100, 10000);

% reading loop
for i = 1 : 100
    % data file expected name
    file_name = strcat(data_prefix, int2str(i), '.csv');

    % check if there is a data file to the index i
    if ~exist(file_name, 'file')
        break;
    end

    % read csv data file
    data = csvread(file_name, 1, 0);

    % get the iterations number (step)
    if ~exist('N', 'var') || N > size(data, 1)
        N = size(data, 1);
    end

    % get positions, velocities and control from data
    x_temp(i, :, 1:N) = data(1:N, 2:4)';
    v_temp(i, :, 1:N) = data(1:N, 5:7)';
    u_temp(i, :, 1:N) = data(1:N, 8:10)';

    % get the coverage and communication radius
    r_cov_temp(i, 1:N) = data(1:N, 11)';
    r_com_temp(i, 1:N) = data(1:N, 12)';

    % get the sampling time
    dt = data(1, 13);

end

% check if there is data file
if i >= 1
    % get the number of robots
    n = i - 1;

    % fit the position array size to the number of robots and iterations
    x = x_temp(1:n, :, 1:N);
    v = v_temp(1:n, :, 1:N);
    u = u_temp(1:n, :, 1:N);
    r_cov = r_cov_temp(1:n, 1:N);
    r_com = r_com_temp(1:n, 1:N);
end

