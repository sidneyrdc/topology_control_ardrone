/**
 * gps.h
 */

#ifndef _GPS_H_
#define _GPS_H_

//#include <termios.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <fcntl.h>
//#include <pthread.h>
//#include <vector.h>
//#include <drk.h>
//#include <sys/syscall.h>
//#include <sched.h>
//#include <drk_sched.h>
#include <time.h>
#include <stdint.h>

///* Serial constants */
//#define	BAUDRATE	B115200
//#define DATABITS	CS8	
//#define STOPBITS	CSTOPB
//#define PARITYON	0
//#define PARITY		0

#define EARTH_RADIUS 6368913 // lets use the radius at 41.17º lat
#define LAT_RADIUS   6368913 // = R_lon,  lat = 41.17º . NORTH SOUTH deltas
#define LON_RADIUS   4801205 // = a.cos( lat ) ,  lat = 41.17º. WEST EAST deltas

// default BS 
#define BASELAT 41.176752 // feup canteen
#define BASELON -8.596227 // feup canteen


/*
 * R_lon = SQRT( num / den )
 * 
 * num = ( a^2*cos(t) )^2 + ( b^2*sin(t) )^2 
 * den = ( a*cos(t) )^2 + ( b*sin(t) )^2
 * Rt = radius of earth at latitude t                
 * a = semi major radius of earth         = 6378.137 meters
 * b = semi minor radius of earth         = 6356.75231420 meters 
 */
 
 
/* Print error messages */
#define OSS 3



typedef struct {
	double latitude; // best lat ; RTK lat (or raw if not available)
	double longitude; //best lat ;  RTK lon (or raw if not available)
	double altitude; // best alt ; 
	double UTC;
	int8_t quality;
	int8_t num_sats; 


	//newly added on 06/02/13
	int date; 		    // from GPRMC
	float hPrecise;     //from $GPGGA - horizontal dilution of precision 
	double trueTrack;   // from VTG data-Track, degrees
	double groundSpeed; //from VTG data - Speed, Km/hr

	//newly added on 01/ago/15
	double spp_lat ; // single point position lat RAW
	double spp_lon ;  //single point position lon RAW
	
	//newly added on 01/set/15
	double rtk_lat ; // rtk lat RAW
	double rtk_lon ;  //rtk  lon RAW
	
	time_t timestamp ;
	
	uint32_t spp_sample_id; // unique sample id ( == tow )
	uint32_t rtk_sample_id; // unique sample id ( == tow )
} gps_t;



/******************
 * Externs
 *****************/
extern struct global_serial* serial_buf;

/* Accessors for GPS data */
//double GPS_latitude();
//double GPS_longitude();
//double GPS_altitude();
/* Helper function definitions */
double drk_degrees_to_radians(double degrees);
double drk_radians_to_degrees(double radians);
//double drk_nmea_to_decimal(double coordinate);

int drk_gps_get_numsats();
int16_t drk_wait_for_gps();
void _drk_set_basestation();
void drk_calibrate_north();
int drk_calibration_data_get();
void drk_calibration_data_set(int xx, int xn, int yx, int yn, int zx, int zn, int offset, int g_offset);
void drk_calibration_data_print();

/* return lastest gps sample data received */
gps_t drk_gps_data();
//gps_t drk_gps_copy();

/* Check for a GPS fix on the current drone's GPS */
int drk_gps_myfix();

/* Check to see if a given GPS data point was valid */
int _drk_gps_fix(gps_t target);

/* Calculate the distance between two coordinate pairs */
double drk_gps_coordinates_distance (double lat1, double long1, double lat2, double long2);

/* Calculate the distance between two GPS structs */
double drk_gps_struct_distance (gps_t gps1, gps_t gps2);

/* Calculate the distance between current position and a coordinate pair */
double drk_gps_coordinates_mydistance (double target_lat, double target_long);

/* Calculate the distance between current position and a GPS struct */
double GPS_mydistance_Struct (gps_t target);

double drk_gps_target_heading (double my_lat, double my_long, double target_lat, double target_long);
double drk_gps_target_myheading (double target_lat, double target_long);
int drk_cartesian_myheading(double target_lat, double target_long);
int drk_cartesian_heading(double my_lat, double my_long, double target_lat, double target_long);

/* Debugging methods */
void drk_gps_print();

#endif // _GPS_H_
