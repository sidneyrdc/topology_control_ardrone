/**
 * drk.h
 */

#ifndef _DRK_H_
#define _DRK_H_

#include <stdint.h>


/* Initialize all of the DroneRK components */
int drk_init(uint8_t usbport);

int drk_init_emergency();

/* close the app. TODO: it should close the library isntead */
void drk_exit(int status);

/* get ip number */
uint8_t drk_ip_num();

void drk_error( char *msg ) ;

double drk_elapsed_time_secs();
double wrapTo2Pi( double angle_in_degrees ) ; /* convert any angle-in-degrees to range 0-360º */

#include <semaphore.h>
int8_t drk_sem_create( char * sem_name , sem_t **sem_ptr ) ; /* force creation of semaphore by name */
int8_t drk_sem_cleanup( char * sem_name , sem_t *sem_ptr ) ; /* clean semaphore by name */


#endif // _DRK_H_

