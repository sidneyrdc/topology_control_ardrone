/**
 * autonomous.h
 */

#ifndef _AUTONOMOUS_H_
#define _AUTONOMOUS_H_

#include <inttypes.h>

#include "gps.h" /* data types - gps_t */

/*******************************************************************************
 * Data types
 ******************************************************************************/
typedef struct
{
  double tlatitude ;
  double tlongitude ;
  double taltitude ;
  double max_output ; //vector-norm horizontal plane
  double distance_tolerance ;
} gps_waypoint_t ;

/**
 * Bit vector to be used with the translate function
 */
enum TRANSLATE_OPTIONS
{
  DRK_CONTINUE  = 0x00000000, // Keep moving when done
  DRK_HOVER     = 0x00000001, // Return to hover when done
  DRK_STOP      = 0x00000002, // TODO: Aggressively stop when done (not in yet)
  DRK_OVERRIDE  = 0x00000004  // Allow control in lockdown
};

/*******************************************************************************
                            AUTONOMOUS FLIGHT FUNCTIONS
*******************************************************************************/

/**
 * Clean up after thread terminated
 */
void drk_autonomous_close();

/**
 * Pause movement immediately  (used by keyboard thread)
 */
void drk_autonomous_pause();


/**
 * Resume flight (used by keyboard thread)
 */
void drk_autonomous_resume();

/**
 * Returns the state of autonomous flight:
 * PAUSED (1) - no rotor output
 * FLYING (0) - flying to target
 */
uint8_t drk_autonomous_get_state();


/**
 * returns the current waypoint target
 */
gps_waypoint_t drk_autonomous_get_waypoint() ;


/**
 * Gives drone new waypoint to go; overwrites current waypoint
 */
void drk_autonomous_set_waypoint( gps_waypoint_t in_waypoint );

/**
 * initiates autonmous controller
 * returns -1 if fails to init
 */
int8_t drk_autonomous_init();


/**
 * Returns 1 if drone is at target position (+- distance_tolerance)
 * 0 otherwise
 */
uint8_t drk_autonomous_target_reached();


/*
 * set PID controller.
 * Kp_? - porportional ,  Ki_? - integral., Kd_? - differental.
 * ?_h - horizontal , ?_v - vertical
 */
void drk_autonomous_set_PID(double _kp_h, double _ki_h, double _kd_h,  double _kp_v);


void drk_print_waypoint_map( gps_waypoint_t local_waypoint , gps_t gps_sample )  ; 


#endif // _AUTONOMOUS_H_
