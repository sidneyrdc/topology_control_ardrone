/**
 * vector.h
 * Nat Storer - 6/3/2011 
 * Perform simple vector operations for compass calculations 
 */

#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>

/* 3D vector */
typedef struct vector
{
  double x, y, z;
} vector;

/* Cross Product */
void vector_cross(const vector *a, const vector *b, vector *out);

/* Dot Product */
float vector_dot(const vector *a,const vector *b);

/* Normalize */
void vector_normalize(vector *a);

#endif
