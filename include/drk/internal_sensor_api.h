/**
 *  internal_sensor_api.h
 * 	use these functions to: receive and parse navdata,  etc
 */

#ifndef _INTERNAL_SENSOR_API_H_
#define _INTERNAL_SENSOR_API_H_
 
#include <semaphore.h>

#define MAG_FILTER_SAMPLES                  (14)




/* Logging unfiltered values for the paper */
struct raw_sensors
{
    int     raw_acc_x;
    int     raw_acc_y;
    int     raw_acc_z;
    float   phys_acc_x;
    float   phys_acc_y;
    float   phys_acc_z;
};

/* used for updating NAV_DEMO_data */
#include <inttypes.h>
struct drone_sensor
{
    uint32_t state;  
    uint32_t sequence;
    int     battery;
    float   altitude;
    float   absolute_altitude;  
    float   pitch;      
    float   roll;  
    float   yaw;
    float   vx; /* UAV’s estimated linear velocity */
    float   vy; /* UAV’s estimated linear velocity */
    float   vz; /* UAV’s estimated linear velocity */
    float   mag_x;
    float   mag_y;
    float   mag_z;
    float   mag_heading;
    float   mag_heading_gyro;
    float   mag_heading_fusion;
    float   gyro_offset;
    float   compass_offset;
    float   fusion_offset;
    int     mag_filter_index;
    float   mag_heading_samples[MAG_FILTER_SAMPLES];
    struct raw_sensors raw_vals;
}; 

struct compass
{
    int mag_x;
    int mag_y;
    int mag_z;
};

struct vision_tags
{
    int number;
    int type[4];
    int x[4];
    int y[4];
    int width[4];
    int height[4];
    int dist[4];
    float angle[4];
};

struct global_navdata
{
    sem_t *semaphore;
    struct drone_sensor sensor;
    struct vision_tags vision;
};

/*******************************************************************************
                                USEFUL ENUMS
*******************************************************************************/

/* Vision tag types */
enum TAG_TYPE
{
    TAG_TYPE_NONE             = 0,
    TAG_TYPE_SHELL_TAG        = 1,
    TAG_TYPE_ROUNDEL          = 2,
    TAG_TYPE_ORIENTED_ROUNDEL = 4,
    TAG_TYPE_STRIPE           = 8,
    TAG_TYPE_NUM
} TAG_TYPE;

/* Detection types */
enum CAD_TYPE
{
    CAD_TYPE_HORIZONTAL = 0,           /*<! Deprecated */
    CAD_TYPE_VERTICAL,                 /*<! Deprecated */
    CAD_TYPE_VISION,                   /*<! Detection of 2D horizontal tags on drone shells */
    CAD_TYPE_NONE,                     /*<! Detection disabled */
    CAD_TYPE_COCARDE,                  /*<! Detects a roundel under the drone */
    CAD_TYPE_ORIENTED_COCARDE,         /*<! Detects an oriented roundel under the drone */
    CAD_TYPE_STRIPE,                   /*<! Detects a uniform stripe on the ground */
    CAD_TYPE_H_COCARDE,                /*<! Detects a roundel in front of the drone */
    CAD_TYPE_H_ORIENTED_COCARDE,       /*<! Detects an oriented roundel in front of the drone */
    CAD_TYPE_STRIPE_V,
    CAD_TYPE_MULTIPLE_DETECTION_MODE,  /* The drone uses several detections at the same time */
    CAD_TYPE_NUM,                      /*<! Number of possible values for CAD_TYPE */
} CAD_TYPE;




/*******************************************************************************
                                FUNCTION PROTOTYPES
*******************************************************************************/
int drk_ctrl_config();

int drk_sensor_data_init();

void drk_ar_object_tracker_setup();

int drk_sensor_data_wait();

int drk_sensor_data_update(struct drone_sensor *, struct vision_tags *);

int drk_sensor_data_get_int(char *, int);

short drk_sensor_data_get_short(char *, int);


float drk_sensor_data_get_float(char *, int);

void dump_sensors();

int8_t drk_sensor_close(); 


/* Read the incoming navdata from the drone */
void* _drk_ar_nav_sensor_thread(void* args);



#endif // _INTERNAL_SENSOR_API_H_
