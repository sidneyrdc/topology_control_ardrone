#include <stdio.h>	// printf
#include <inttypes.h>	// int16_t etc
#include <sys/mman.h>	/* shm_open */
#include <sys/stat.h>	/* For mode constants */
#include <fcntl.h>	/* For O_* constants */
#include <semaphore.h>	/* semaphore */
#include <unistd.h> /* close */

/* get clock time */
uint64_t getSimClock();

/* printf clock time */
void printSimClock( ) ;

/* init clock by creating a shared memory space for other processes to read clock time */
int8_t initSimClock();

/* pause clock time */
void pauseSimClock();

/* resume clock time */
void resumeSimClock();
