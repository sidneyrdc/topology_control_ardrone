/**
 * ar_configuration.h
 * use this functions to configure the drone , max speeds, etc..
 */

#ifndef _AR_CONFIGURATION_H_
#define _AR_CONFIGURATION_H_

#include <semaphore.h>

#include "drk/internal_sensor_api.h" /* drone_actuator */

/***************************************
 * Structs
 **************************************/

/****************************
 * Declarations
 ***************************/
// Maximum pitch / roll angle. Must be between 0 and 0.52 radians
void drk_ar_change_max_angle (float maximum_angle);

// min Altitude of the drone in millimeters. 50 - max
void drk_ar_change_min_altitude(int minimum_altitude);

// Maximum Altitude of the drone in millimeters. 500 - 5000, or 10000 = no lim
void drk_ar_change_max_altitude (int maximum_altitude);

// Maximum Vertical Speed, in millimeters per second, 200-2000
void drk_ar_change_max_vertical_speed (int maximum_speed);

// Maximum Yaw Speed, in radians per second, 0.7 - 6.11
void drk_ar_change_max_yaw_speed (float maximum_speed);

void drk_ar_set_outdoor_flight();

void drk_ar_flat_trim();

void drk_ar_set_indoor_flight();

void drk_ar_set_outdoor_hull();

void drk_ar_set_indoor_hull();

void drk_ar_calibrate_magnetometer();

void drk_ar_set_hover_oriented_roundel();

#endif // _AR_CONFIGURATION_H_
