/**
 *  internal_sensor_api.h
 * 	use these functions to: receive and parse navdata,  etc
 */

#ifndef _INTERNAL_ACTUATOR_API_H_
#define _INTERNAL_ACTUATOR_API_H_

#include <inttypes.h>


typedef union _float_or_int_t {
  float f;
  int32_t i;
} float_or_int_t;

struct drone_actuator
{
    int takeoff_flag;		/* 1 = take off, -1 = landing, 0 = flying */
    int emergency_flag;
    int pcmd_flag;
    float_or_int_t roll;	/* Left/Right angles */
    float_or_int_t pitch;	/* Forth/Back angles */   
    float_or_int_t gaz;		/* vertical speed, + means rise, - means go down */
    float_or_int_t spin;	/* angle speed, + spin right, - spin left */ 
    int magneto_calib;		/* calibrate magnetometer */
    int flat_trim;			/* clibrate accelerometers */
    
};

struct global_actuator //TODO:move this 
{
    sem_t *semaphore;
    sem_t *time_sem;
    struct drone_actuator actuator;
    long int global_timestamp;
};


/* Send a command to the drone */
int drk_actuator_init( void ) ;
int8_t drk_actuator_close() ;
int drk_actuator_update(struct drone_actuator *p);
int drk_send_at_command(char *send_command);

#endif
