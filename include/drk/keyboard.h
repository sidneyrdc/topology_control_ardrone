/********* 
 * keyboard thread 
 * ***********/
#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include <inttypes.h>

int8_t drk_keyboard_init();
int8_t getLockDown() ;

#endif
