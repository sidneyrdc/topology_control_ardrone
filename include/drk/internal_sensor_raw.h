/**
 * internal_sensor_raw.h
 */

#ifndef _INTERNAL_SENSOR_RAW_H_
#define _INTERNAL_SENSOR_RAW_H_

#include <stdint.h>

#include "internal_sensor_api.h"

/* List of LED animations */
enum LED_ANIMATION
{
	BLINK_GREEN_RED,
	BLINK_GREEN,
	BLINK_RED,
	BLINK_ORANGE,
	SNAKE_GREEN_RED,
	FIRE,
	STANDARD,
	RED,
	GREEN,
	RED_SNAKE,
	BLANK,
	RIGHT_MISSILE,
	LEFT_MISSILE,
	DOUBLE_MISSILE,
	FRONT_LEFT_GREEN_OTHERS_RED,
	FRONT_RIGHT_GREEN_OTHERS_RED,
	REAR_LEFT_GREEN_OTHERS_RED,
	REAR_RIGHT_GREEN_OTHERS_RED,
	LEFT_GREEN_RIGHT_RED,
	LEFT_RED_RIGHT_GREEN
};

/* Thread-safe altitude accessor */
double drk_ultrasound_raw();

/* Ultrasound altitude reading compensated for tilt */
double drk_ultrasound_altitude();

double drk_abs_altitude();

//computes the altitude from the sea when the node runs this function. saves it in the global var named <zero_altitude>
void drk_calibrate_abs_altitude(uint16_t dft);

double drk_lpf_abs_altitude();

/* Thread-safe pitch accessor */
double drk_drone_pitch_get();

/* Thread-safe roll accessor */
double drk_drone_roll_get();

/* Thread-safe yaw accessor. This is RELATIVE! TODO:relative to what? */
double drk_drone_yaw_get();

double drk_drone_speed_x_get();

double drk_drone_speed_y_get();

double drk_drone_speed_z_get();

struct compass drk_compass_data();

/* Current BEST heading calculation. If you find better, edit this */
double drk_heading();

double drk_mag_filtered_heading();

double drk_tilt_comp_heading();

double _drk_tc_struct_heading(struct drone_sensor * buf);

void drk_magneto_raw(double *magx, double *magy, double *magz);

double drk_compass_heading();

float drk_fusion_heading();

// Tells the drone that its current heading is north for calibration 
void drk_calibrate_north();

// Yaw value compensated for offset from compass, ABSOLUTE if calibrated 
double drk_gyro_heading();

// Zero the gyro by assuming your current heading is north 
void drk_gyro_zero();

// Calibrate the gyro offset, assuming the compass is correct 
void drk_gyro_calibrate();

//COMPASS CALIBRATION stuff
void drk_calibration_data_set(int xx, int xn, int yx, int yn, int zx, int zn, int offset, int g_offset);

int drk_calibration_data_get();

/*******************************************************************************
                                UTILITIES
*******************************************************************************/

/* Play an animated sequence on the LEDs */
int drk_play_LED_animation(enum LED_ANIMATION animation, float frequency, int duration);

/* Prints and shows a graph of the battery */
void drk_print_battery();

int drk_get_battery();

struct raw_sensors drk_get_raw_sensors();

void drk_set_heading_offsets(double gyro, double fusion, double compass);

#endif // _INTERNAL_SENSOR_RAW_H_
