/******************************************************************************
 * Constants and Macros <Header>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2017 Out 11 18:41:59
 * Info: This file define global constants and macros to Topology Control
 * Algorithm.
 *****************************************************************************/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define ARMA_DONT_USE_CXX11             // to avoid armadillo incompatibility with C++11
#define ARMA_DONT_USE_WRAPPER           // to use -llapack and -lblas instead of -larmadillo
#define ARMA_USE_LAPACK                 // to use LAPACK library (some functions, e.g. inv, need that)
//#define ARMA_USE_BLAS                   // to use BLAS library (turn matrix multiplication faster)

#define MSG_SIZE 2048                   // received message maximum size
#define HIST_WIN_SIZE 10                // message history window size

#define GEO_MAJ_AXIS 6378137.0          // ellipsoid semi-major axis (WGS84) (m)
#define GEO_MIN_AXIS 6356752.314245     // ellipsoid semi-minor axis (WGS84) (m)

#define PI 3.1415926535897              // number π

/*[> reference to relative cartesian coordinates <]*/
#define BASE_LAT 41.176752              // feup canteen (º)
#define BASE_LON -8.596227              // feup canteen (º)

/*#define LAT_RADIUS 6368913.0*/
/*#define LON_RADIUS 4801205.0*/

#define UDP_PORT_BASE_SRC_80211 40000
#define UDP_PORT_BASE_SRC_ACT 41000
#define UDP_PORT_BASE_RX_ALL 50000
#define UDP_PORT_BASE_TX_80211 60000
#define UDP_PORT_BASE_TX_SENSOR 61000

#endif

