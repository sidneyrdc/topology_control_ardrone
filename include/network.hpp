/******************************************************************************
 * Network Module for Topology Control Algorithm <Header>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2016 Nov 22 15:18:02
 * Info: This file contains the header of the network communication algorithms
 *****************************************************************************/

#ifndef NETWORK_H
#define NETWORK_H

#include <constants.hpp>
#include <thread>
#include <vector>
#include <armadillo>
#include <inttypes.h>

/* return an array of bytes from any data input */
unsigned char *get_raw(const void *data, size_t data_len);

/* print raw bytes from a input data */
void print_raw(const void *data, size_t data_len);

/* control struct */
typedef struct {
    float x;                    // x axis (4 Bytes)
    float y;                    // y axis (4 Bytes)
    float z;                    // z axis (4 Bytes)
    float yaw;                  // θ angle (4 Bytes)
} pose_t;

/* state struct */
typedef struct {
    uint16_t n_bytes;           // struct length in bytes (2 Bytes)
    uint8_t id;                 // sender id (1 Byte)
    uint8_t n1_size;            // 1-hop neighborhood size (1 Byte)
    uint8_t *n1;                // 1-hop neighborhood (n1_size ∙ 1 Bytes)
    pose_t *x;                  // state array ((1 + n1_size) ∙ 3 ∙ 4 Bytes)
    pose_t *u;                  // control array ((1 + n1_size) ∙ 3 ∙ 4 Bytes)
    float *r_cov;               // coverage radius array ((1 + n1_size) ∙ 4 Bytes)
    float *r_com;               // communication radius array ((1 + n1_size) ∙ 4 Bytes)
} state_t;

unsigned char *serialize(state_t data);

state_t deserialize(unsigned char *raw_data);

/*[> state message class <]*/
//class state_t {
//public:
    //// constructor
    //state_t(unsigned int id, size_t n1_size);

    //// destructor
    //~state_t();

    //// return a byte array from state input
    //void *state2bytes();

    //// return state from byte array
    //void bytes2state(const void *byte_array);

    //// public variables
    //uint8_t id;                     // sender id (1 Byte)
    //uint8_t n1_size;                // 1-hop neighborhood size (1 Byte)
    //uint8_t *n1;                    // 1-hop neighborhood set (n1_size ∙ 1 Byte)
    //pose_t *x;                      // state array ((1 + n1_size) ∙ 3 ∙ 4 Bytes)
    //pose_t *u;                      // control array ((1 + n1_size) ∙ 3 ∙ 4 Bytes)
    //float *r_cov;                   // coverage radius array ((1 + n1_size) ∙ 4 Bytes)
    //float *r_com;                   // communication radius array ((1 + n1_size) ∙ 4 Bytes)
    //size_t n_bytes;                 // number of bytes in the state_t
    //unsigned char *raw;             // raw data

//private:
//};

/* thread to receive UDP messages */
class RXThread {
public:
    // constructor
    RXThread(unsigned int port);

    // destructor
    ~RXThread();

    // get received message buffer
    void get_msg(float timeout, std::vector<void *> &msg_pool, std::vector<unsigned int> &msg_len);

    // to stop the thread
    void stop();

private:
    bool new_msg;                               // indicator for received messages
    bool run;                                   // indicator to stop the thread
    bool get_msg_lock;                          // get message locker
    bool rec_msg_lock;                          // receive message locker
    float dt;                                   // time difference between get_msg calls

    std::vector<void *> msg_pool;               // all received messages buffer
    std::vector<unsigned int> msg_len;          // number of bytes of each received message

    int udpr;                                   // UDP receiver socket
    std::thread *rx_thread;                     // pointer to receiver thread

    // receiver loop
    void rx_loop();
};

/* thread to send UDP messages */
class TXThread {
public:
    // constructor
    TXThread(float dt, unsigned int src_port, std::vector<const char *> dst_ip, std::vector<short unsigned int> dst_port);

    // destructor
    ~TXThread();

    // send a message
    void send_msg(const void *msg, unsigned int msg_size);

    // to stop the sender thread
    void stop();

private:
    const void *msg;                            // message to send
    unsigned int msg_len;                       // size of message to send
    bool run;                                   // indicator to stop the thread

    int udps;                                   // UDP sender socket
    std::thread *tx_thread;                     // pointer to sender thread

    // sender loop
    void tx_loop(float dt, std::vector<const char *> dst_ip, std::vector<short unsigned int> dst_port);
};

/* received message history */
class MHistory {
public:
    // constructor
    MHistory(unsigned int nodes_num);

    // destructor
    ~MHistory();

    // mark received message to a node
    void mark_msg(unsigned int id);

    // advance with the current time slot
    void inc_time();

    /* variables */
    arma::umat hist;            // history array

private:
    unsigned int t_slot;        // time slot
    unsigned int nodes_num;     // number of nodes
};

#endif

