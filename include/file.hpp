/******************************************************************************
 * External Files Tools <Header>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2017 Out 10 20:04:10
 * Info: This file contains the header of the functions to manipulate external
 * archive files, as text and matlab files.
 *****************************************************************************/

#ifndef FILE_H
#define FILE_H

#include <constants.hpp>
#include <utils.hpp>
#include <armadillo>
#include <stdio.h>
#include <string.h>

// default file types
#define CSV 1
#define MAT 2
#define TXT 3

/*
 * Output data file main class
 */
class DataFile {
public:
    // class constructor
    DataFile(std::string file_name, int file_type);

    // class destructor
    ~DataFile();

    // write data in the archive file
    void write(int step, arma::rowvec x, arma::rowvec v, arma::rowvec u, float r_com, float r_cov, float dt);

private:
    char file_name[50];             // output file name
    int file_type;                  // output file type

    FILE *data_file;                // pointer to the data file handle

    // open data file
    void open();

    // close data file
    void close();

};

#endif

