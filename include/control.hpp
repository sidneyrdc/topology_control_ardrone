/******************************************************************************
 * Motion Control Algorithms <Header>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2017 Mar 03 16:15:30
 * Info: This file contains the header of the motion control algorithms
 *****************************************************************************/

#ifndef CONTROL_H
#define CONTROL_H

#include <constants.hpp>
#include <QuadProg++.hh>
#include <armadillo>
#include <utils.hpp>

/*
 * Control main class
 */
class control {
public:
    // class constructor
    control(unsigned int id,
            float p,
            float dt,
            float ux_max,
            float ux_min,
            float ua_max,
            float ua_min,
            arma::vec r_com,
            arma::vec r_cov,
            arma::vec gamma);

    // class destructor
    ~control();

    // first order model predictive motion control (CMC)
    arma::vec mpc_1st_order(arma::mat x, arma::mat u, arma::uvec A, arma::uvec H);

    // second order model predictive motion control (CMC)
    arma::vec mpc_2nd_order(arma::mat x, arma::mat v, arma::mat u, arma::uvec A, arma::uvec H);

    // simple proportional control
    arma::vec p_motion_control(arma::vec x, arma::vec r, arma::vec u);
private:
    unsigned int id;            // robot id assigned to the control

    unsigned int p;             // MPC prediction horizon size
    float dt;                   // integration step (s)

    float ux_max;               // upper bound for linear control signal
    float ux_min;               // lower bound for linear control signal
    float ua_max;               // upper bound for angular control signal
    float ua_min;               // lower bound for angular control signal

    arma::vec r_com;            // communication radius array
    arma::vec r_cov;            // coverage radius array

    arma::vec gamma;            // MPC weights array

    arma::mat T;                // triangular matrix of order p
    arma::mat T2;               // square of T
    arma::mat TI;               // inverse of T
    arma::mat I;                // identity matrix
    arma::mat P;                // diagonal matrix of (1,...,p)

    // translate coordinates between different references
    arma::vec rot(arma::vec x, float );
};

#endif

