/******************************************************************************
 * Extra functions to network topology control algorithm <Header>
 *
 * Author: Sidney Carvalho - sydney.rdc@gmail.com
 * Last Change: 2016 Dez 13 17:22:33
 * Info: This file contains the header of the auxiliary functions used in the
 * network topology control algorithm.
 *****************************************************************************/

#ifndef UTILS_H
#define UTILS_H

#include <constants.hpp>
#include <math.h>
#include <network.hpp>
#include <QuadProg++.hh>

extern "C" {
#include <drk/gps.h>                // read gps samples
}

/*
 * Geodetic Transformation
 *
 * Info: transform from geodetic (latitude, longitude, ellipsoidal heigh) to
 * geocentric coordinates (X, Y, Z). Geodetic are obtained from WGS84 and geocentric
 * are obtained from ECEF.
 */
pose_t lla2ecef(gps_t gps_data);

/*
 * Normal of a latitude
 *
 * Info: calculates the distance from the earth's surface to the Z-axis along
 * the ellipsoid normal. lat is the latitude and must be provided in radians.
 */
double geo_norm(float lat);

/*
 * Geodetic data to local coordinates
 *
 * Info: transform from geodetic (latitude, longitude, altitude) to local
 * Cartesian coordinates (X, Y, Z).
 */
pose_t lla2xyz(gps_t gps_data);

/*
 * Compass angle to cartesian angles
 *
 * Info: Converts angles from compass referential to cartesian referential.
 */
float comp2cart(float comp_angle);

/*
 * Templates of functions
 */

// print a matrix of QuadProgPP type
template <typename T> void print_matrix(QuadProgPP::Matrix<T> input) {
    // get the matrix sizes
    const unsigned int n = input.nrows();
    const unsigned int m = input.ncols();

    for(unsigned int i = 0; i < n; i++) {
        for(unsigned int j = 0; j < m; j++) {
            if(j < m-1) std::cout << input[i][j] << " ";
            else std::cout << input[i][j];
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// print a vector of QuadProgPP type
template <typename T> void print_vector(QuadProgPP::Vector<T> input) {
    // get the vector size
    const unsigned int n = input.size();

    for(unsigned int i = 0; i < n; i++) {
        if(i < n-1) std::cout << input[i] << " ";
        else std::cout << input[i];
    }
    std::cout << std::endl;
}

#endif

