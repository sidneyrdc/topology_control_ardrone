#!/bin/bash

# kill older instances when ctrl-c is pressed
trap 'killall main; killall uavworld_simulator' 2

# run four instances of topology control
./bin/main 1 &
./bin/main 2 &
./bin/main 3 &
./bin/main 4

