% clc
% analyze events

N=5 ;
PERIOD = 600;
palete=lines(N);
SRC_COL=2;
INIT_COL=4;
END_COL=5;
list_logs = dir('*.log') ;
filename = list_logs(end).name ;

%while (1)
% date_str = '17h05m37s' _1;
% rawdataset = dlmread(...
%     sprintf('sim_2016-11-16_%s_sim.log', date_str ),...
%     ' ', 1, 0);
rawdataset = dlmread(...
    filename,...
    ' ', 1, 0);
fprintf('Pkts: %d\n', length(rawdataset) )


dataset80211 = rawdataset(rawdataset(:,6)==1,1:end-1) ;
timeref = 0 ;% floor(dataset80211(1,INIT_COL)/1e3/)*400*1e3 ;

% timeref= dataset80211(1,2);
dataset80211(:,INIT_COL) = dataset80211(:,INIT_COL)- timeref;
dataset80211(:,END_COL) = dataset80211(:,END_COL) - timeref;

% datasetEsensor = rawdataset(rawdataset(:,6)==2,1:end-1);
% datasetEsensor(:,INIT_COL)=datasetEsensor(:,INIT_COL)- timeref;
% datasetEsensor(:,END_COL)=datasetEsensor(:,END_COL)- timeref;
%
% datasetIsensor = rawdataset(rawdataset(:,6)==3,1:end-1);
% datasetIsensor(:,INIT_COL)  = datasetIsensor(:,INIT_COL)- timeref;
% datasetIsensor(:,END_COL)   = datasetIsensor(:,END_COL)- timeref;


% for i=0:1
%     plot([1 1]*i*400 , [0 100],'-k')
%     plot([1 1]*i*400+100 , [0 100],'-b')
% end
%% prepare plot
% clf
hold on
grid on
xlabel('Time in the round (ms)')
ylabel('Epoch (s)')
%axis([ 90 251 0 700 ])
% axis([ 2580 2590 650 700 ])
legend_list = cell(5,1) ;
for i = 1:N
    plot(-1,-1, '-','linewidth',3,'color', ...
        palete(i,:) )
    legend_list{i}=num2str(i);
end
legend(legend_list,...
    'Location',...
    'Southwest')
title('TDMA events registered at the Simulator')

%% plot events-80211
for src=1:N
    subdata = dataset80211( dataset80211(:,SRC_COL) == src ,:) ;
    x1 = mod(subdata( : , INIT_COL )/1e3 ,PERIOD);%/ 1e3  ;
    x2 = x1 + diff( subdata( : ,[ INIT_COL END_COL ] ),1,2) /1e3  ;
    x= [x1 x2 ] ;
    y = subdata( : , INIT_COL )/1e6 * [ 1 1 ];
    
    line(...
        x' ,...
        y', ...
        'linewidth',3,'color', ...
        palete(src, : ) )
    
end

%pause( 1 )
%end

%% plot E sensor
% for i=1:size(datasetEsensor,1)
%     plot( ...
%         datasetEsensor(i,END_COL)/1e3 ,...
%         70 ,...
%         'sk','markersize',5, 'markerfacecolor','k')
% end

%% plot E sensor
% for i=1:size(datasetIsensor,1)
%     plot( ...
%         datasetIsensor(i,END_COL)/1e3 ,...
%         60 ,...
%         'sg','markersize',5, 'markerfacecolor','g')
% end
