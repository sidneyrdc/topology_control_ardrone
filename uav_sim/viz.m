%% VIZUALIZER for the the drone_simulator

PLOT_PERIOD = 0.5e6 ; % period to plot pdr

%% Opening the sim clock (mem share)
try
    clock_memoryshare = memmapfile('/dev/shm/clock_memspace') ;
catch
    disp('please run ./simulator first')
    return
end

%% Opening the sim layout (mem share)
try
    layout_memoryshare = memmapfile('/dev/shm/layout_memspace') ;
catch
    disp('please run ./drone_simulator first')
    return
end

%% from layout.h %%%%%%%%%%%%%%%%%%%%%%%%%%
% typedef struct {
%   float X ; /* 4 bytes */
%   float Y ; /* 4 bytes */
%   float Z ; /* 4 bytes */
% } truple_t ; /* 12 bytes */
%
% typedef struct {
%   /* current physics */
%   truple_t    position ; /* 12 bytes */
%   truple_t    velocity ; /* 12 bytes */
%   float yaw_degrees ;  /* 4 bytes */
%   /* current actuation */
%   truple_t    thrust ; /* 12 bytes */
%   float       torque ; /* 4 bytes */
%
% } state_t ; /* 44 bytes */
%
% typedef struct {
%   state_t state[MAX_NUM_DRONES+1] ; // lets ignore node 0
% } layout_t ; /* (10+1)*44 bytes = 484 bytes */
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% semaphore
% try
%     semaphore('w'); %wait
%     semaphore('p'); %post
% catch
%     mex -O -v semaphore.c % compile in case it wasn't
% end

%% get data
BYTE_POSITION = 0 ;
BYTE_VELOCITY = BYTE_POSITION + 12 ;
BYTE_YAW = BYTE_VELOCITY + 12 ;
BYTE_THRUST = BYTE_YAW + 4 ;
BYTE_TORQUE = BYTE_THRUST + 12 ;
STATE_SIZE = 44 ;
TRUPLE_SIZE = 12 ;
NUM_DRONES = 5 ;
% position = zeros(NUM_DRONES,3) ;
colorcode = lines( NUM_DRONES+1 ) ;

%% cycle to show movement
clf
set( gcf , 'position', [ 2130 432 670 622 ] )
t = cell(1,10) ;
position = cell(1,10) ;
for drone_id = 1 : NUM_DRONES
    t{drone_id} = 0 ;
end
max_xy= 30;
while ( 1 )
    for drone_id = 1 : NUM_DRONES
        t{drone_id} = t{drone_id} + 1 ;
        range = drone_id * STATE_SIZE + ( BYTE_POSITION + (1:TRUPLE_SIZE) ) ;
        position{drone_id}( t{drone_id} , 1 : 3 ) = double(typecast( layout_memoryshare.Data( range ) , 'single' )) ;
        range = drone_id * STATE_SIZE + ( BYTE_YAW + (1:4) ) ;
        yaw{drone_id}( t{drone_id} ) = double(typecast( layout_memoryshare.Data( range ) , 'single' )) ;
    end

    clf
    hold on
    grid on
    quad_axis = max_xy/20 ;
    for drone_id = 1 : NUM_DRONES
        plot(   position{drone_id}( 1:t{drone_id} , 1 ) , ...
                position{drone_id}( 1:t{drone_id} , 2 ) , ...
                '-.', 'color' , colorcode ( drone_id , : ) )

        tmpyaw_deg = yaw{drone_id}( t{drone_id} )  ;
        tmpyaw_rad = tmpyaw_deg*pi/180 ;
        R = [ cos( tmpyaw_rad ) , -sin( tmpyaw_rad) ; sin( tmpyaw_rad) , cos( tmpyaw_rad ) ] ;

        % drones 4 rotors
        x = position{drone_id}( t{drone_id} , 1 ) ;
        y = position{drone_id}( t{drone_id} , 2 ) ;
        offset = pi/4 ;
        for i = 1 :4
            offset = offset + pi/2 ;
            xx(i) = x + quad_axis*cos(tmpyaw_rad+offset) ;
            yy(i) = y + quad_axis*sin(tmpyaw_rad+offset) ;
        end
        plot( x   , ...
              y   , ...
                'ko', ...
                'markerfacecolor' , colorcode ( drone_id , : ) ,...
                'markersize' , 10 )

        plot( xx  , ...
              yy  , ...
                'ko', 'markerfacecolor' , colorcode ( drone_id , : ) )

        % front tip
        plot(   position{drone_id}( t{drone_id} , 1 ) + quad_axis*cos(tmpyaw_rad  ) , ...
                position{drone_id}( t{drone_id} , 2 ) + quad_axis*sin( tmpyaw_rad )  , ...
                'ko', ...
                'markerfacecolor' , 'k',...
                'markersize', 4 )

        % label
        text(   position{drone_id}( t{drone_id} , 1 ) + -0.5*quad_axis , ...
                position{drone_id}( t{drone_id} , 2 ) + 0*quad_axis   , ...
                sprintf('%01d', drone_id  ) , 'color','k' ) ;

        % coverage radius
        uistack(viscircles([x, y], 25, 'EdgeColor', 'k', 'LineWidth', 0.1, 'LineStyle', '-.'), 'bottom');

        %print altitude
        text(   position{drone_id}( t{drone_id} , 1 ) - quad_axis , ...
                position{drone_id}( t{drone_id} , 2 ) - 2*quad_axis  , ...
                sprintf('%0.1fm', position{drone_id}( t{drone_id} , 3 ) ) ,...
                'color','k' ) ;

        max_xy= max([ abs(x),abs(y), 30, max_xy]) ;
    end

    axis([-1 1 -1 1] * ceil(max_xy/10)*10 )
    axis square
    axis equal

    title(...
    sprintf(...
        'Clock: %0.4fs' , ...
        1/1e6*single( getSimClock( clock_memoryshare ) ) ...
    ) ...
    )
    xlabel('West-East (m)')
    ylabel('North- South (m)')
    pause(0.35)
end

